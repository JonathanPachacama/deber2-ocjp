package com.company;



public class Main {

    public static void main(String[] args) {

        //INTERFACE
        //Persona p = new Persona();
        //hacerCantar(p);


        //ENCAPSULAMIENTO

        Encapsulamiento enc = new Encapsulamiento();
        enc.setVar(5);
        System.out.println("la variable es:"+enc.getVar());

        //SOBRECARGA DE METODOS
        Sobrecarga s = new Sobrecarga();
        s.cambiarEdad(25, "Jonathan",1.6);
        s.cambiarEdad(1.8, " Jonathan");
        s.cambiarEdad(10 ,1.2);
        s.cambiarEdad("Estefania ", true);


        //SOBREESCRITURA DE METODOS
        Sobreescritura se = new Sobreescritura();
        se.cambiarEdad(21,"estefy",1.5);

        //CONSTRUCTORES
        Persona p= new Persona("Stefy",21,"Mujer");


        //ENUMS
        ArticuloEnum a = ArticuloEnum.NUEVO;
        ArticuloEnum b = ArticuloEnum.USADO;
        ArticuloEnum c = ArticuloEnum.AVERIADO;
        //nombre del enumerador
        System.out.println("el estado del articulo es: "+a.name());
        System.out.println("el estado del articulo es: "+b.name());
        System.out.println("el estado del articulo es: "+c.name());
        //posiscion del enumerador
        System.out.println("La posicion del enum NORMAL es: "+c.ordinal());
        //enume disponibles
        for(ArticuloEnum d:ArticuloEnum.values()){
            System.out.print(d.toString()+"-");
        }


    }

    public static void hacerCantar(MusicoInterface c)
    {
        c.cantar();
    }

}
